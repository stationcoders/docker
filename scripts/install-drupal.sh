#!/bin/bash
set -e

if [ ! "$(ls -A $PROJECT_LOCAL_PATH)" ]; then
  # docroot is empty better clone the repo here before install the website.
  git clone $PROJECT_REPO_URI $PROJECT_LOCAL_PATH || exit 1
fi

# Create files folder
cd $PROJECT_LOCAL_PATH
# On platform there is no htaccess
# cp .htaccess.local .htaccess
composer install
# Create a settings.pipeline.php with (db and user = drupal) and password = password
ls -lh
ls -lh $PROJECT_LOCAL_PATH
cp $PROJECT_LOCAL_PATH/settings.pipeline.php $PROJECT_DOCROOT/sites/default/settings.local.php

cd $PROJECT_DOCROOT
# IMPORTANT
# add this line in each drush alias specific for pipeline:
# ssh_options -i ~/.ssh/id_rsa -o StrictHostKeyChecking=no
drush sql-sync @${PROJECT_NAME}.${PROJECT_BRANCH} default -vvv -y || exit 1
drush updb -y
drush cr -y
drush config-import -y
# clean drupal cache again
drush cr

# Setup permissions
sudo chmod -Rv 777 sites/default/files
sudo chown -Rv pipelineuser $PROJECT_LOCAL_PATH
