# This is already available for clone dir
export PROJECT_LOCAL_PATH="/var/www/testspipeline"
# This have to be created in bitbucket repo
export PROJECT_PRIVATE_KEY=$PRIVATE_KEY
export PROJECT_DOCROOT="${PROJECT_LOCAL_PATH}/docroot"
export PROJECT_REPO_URI="git@bitbucket.org:stationcoders/docker.git"
export PROJECT_BRANCH="master"
export PROJECT_URL="localhost"
export PROJECT_NAME="drupalTest"
# Add all the remote hosts to knwo hosts
# ssh-keyscan ssh.eu.platform.sh >> ~/.ssh/known_hosts
# ssh-keyscan git.eu.platform.sh >> ~/.ssh/known_hosts
# Setup webserver and drupal
# sudo -E sh scripts/configure-private-key.sh
# -E preserver environement variables when sudo
# sudo -E sh scripts/init-webserver.sh
sudo -E sh scripts/install-drupal.sh
# Run tests
# sh scripts/init-selenium.sh
# sh scripts/start-drupal-testing.sh