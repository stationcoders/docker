#!/bin/bash
# behat configuration
cd $PROJECT_LOCAL_PATH
# Todo: Create a file in the project repo called behat-pipeline.yml.example
# This file already contain local_path = /var/www/drupal and siteurl=localhost
cp behat-pipeline.yml.example behat-local.yml
sed -i 's/TOBEREPLACED/$PROJECT_LOCAL_PATH/g' behat-local.yml
vendor/behat/behat/bin/behat -c behat.yml features -v
