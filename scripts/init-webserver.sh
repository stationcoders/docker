#!/bin/bash
set -e

# Configure apache virtual host (OLD VERSION)
# cat > /etc/apache2/sites-available/000-default.conf <<EOF
# <VirtualHost *:80>
#   ServerAdmin master@localhost
#   DocumentRoot $PROJECT_DOCROOT
#   <Directory "$PROJECT_DOCROOT">
#     Options FollowSymLinks
#     AllowOverride All
#     Order allow,deny
#     Allow from all
#   </Directory>
# </VirtualHost>
# EOF

# Configure apache virtual host
cat > /etc/apache2/sites-available/000-default.conf <<EOF
<VirtualHost *:80>
  ServerAdmin master@localhost
  DocumentRoot $PROJECT_DOCROOT
  <Directory "$PROJECT_DOCROOT">
    Options FollowSymLinks
    AllowOverride All
    Require all granted
  </Directory>
</VirtualHost>
EOF

# Enable default virtual host
a2ensite 000-default.conf
# restart apache just when all the permissions and folder are configured
sudo /etc/init.d/apache2 restart

# Configure mysql and create user and db
/etc/init.d/mysql start
mysql -uroot -e 'create database drupal'
mysql -uroot -e "CREATE USER 'drupal'@'localhost' IDENTIFIED BY 'password'";
mysql -uroot -e "GRANT ALL PRIVILEGES ON * . * TO 'drupal'@'localhost';";
