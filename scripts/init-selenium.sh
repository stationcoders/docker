#!/bin/bash
# SELENIUM - CHROME
export DISPLAY=:99
Xvfb :99 -shmem -screen 0 1366x768x16 &
wget http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.0.jar
wget http://chromedriver.storage.googleapis.com/2.36/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
# force a crhome update to the latest version
wget -N https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -P ~/
sudo dpkg -i --force-depends ~/google-chrome-stable_current_amd64.deb
sudo apt-get -f install -y
sudo dpkg -i --force-depends ~/google-chrome-stable_current_amd64.deb
# start selenium
java -client -jar selenium-server-standalone-2.53.0.jar -Ddocrootdriver.chrome.driver='chromedriver' -browser browserName=chrome,maxInstances=5,platform=LINUX &
# - wait until slenium start and open port 4444
sleep 10s
