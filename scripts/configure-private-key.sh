#!/bin/bash

# Configure private ssh for this server.
if [ ! -d "~/.ssh" ]; then
  mkdir ~/.ssh
fi

echo $PROJECT_PRIVATE_KEY > ~/.ssh/id_rsa.tmp
base64 -di ~/.ssh/id_rsa.tmp > ~/.ssh/id_rsa
chown -Rv pipelineuser ~/.ssh/
chmod 600 ~/.ssh/id_rsa