#!/bin/bash
# ###
# Usefull list of commands to be placed in other scripts
# or pipeline directly to print debug infos
# ###
#
# ps aux
# netstat -nat
# -q parameter means quite. This is needed for debugging reasons
# without it, its not possible print the output of the error log
# wget -q http://localhost/ || true
# print any error if presents
# sudo tail -n 100 /var/log/apache2/error.log
# print the output of the downloaded page
# cat index.html