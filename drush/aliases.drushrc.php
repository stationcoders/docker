<?php

$aliases['pipeline.projectname.branch'] = array (
  'root' => '/app/docroot',
  'platformsh-cli-auto-remove' => true,
  'uri' => 'projecturl.com',
  'remote-host' => 'projecthostname.com',
  'remote-user' => 'usernameforlogin',
  'ssh-options' => '-i /home/pipelineuser/.ssh/id_rsa -o StrictHostKeyChecking=no'
);
